import React from 'react'
import { Link, NavLink } from 'react-router-dom'

export const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
      <Link 
        className="navbar-brand" 
        to="/"
      >
        Inicio
      </Link>
      <div className="navbar-collapse">
        <div className="navbar-nav">
          <NavLink 
            className={ ({ isActive }) => 'nav-item nav-link' + (isActive ? 'active' : '')}
            to="/home"
          >
            Home
          </NavLink>
          <NavLink 
            className={ ({ isActive }) => 'nav-item nav-link' + (isActive ? 'active' : '')}
            to="/favoritos"
          >
            Favoritos
          </NavLink>
        </div>
      </div>
    </nav>
  );
}