import React, { useContext } from "react";
import { PersonajeContext } from "../../contexts/PersonajeContext";

export const HomeScreen = () => {
  const { personajes } = useContext(PersonajeContext);
  return (
    <div>
      <h1>HomeScreen</h1>
      <span>{personajes}</span>
    </div>
  );
}