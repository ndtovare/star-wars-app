import React, { createContext, useEffect, useState } from "react";
import { PersonajeService } from "../services/PersonajeService";

export const PersonajeContext = createContext();

export const PersonajeContextProvider = (props) => {
  const personajeService = new PersonajeService();
  const [personajes, setPersonajes] = useState([]);
  useEffect(() => {
    personajeService.getAll().then(data => setPersonajes(data));
  }, [personajeService, personajes]);
  return (
    <PersonajeContext.Provider
      value = {{
        personajes
      }}
    >
      {props.children}
    </PersonajeContext.Provider>
  )
}