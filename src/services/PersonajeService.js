import axios from "axios";

export class PersonajeService {
  baseUrl = 'https://swapi.dev/api/';

  getAll(personaje) {
    return axios.get(this.baseUrl + 'people/').then(res => res);
  }
}