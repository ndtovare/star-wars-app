import React from 'react';
import { AppRouter } from './routers/AppRouter';

function App() {
  return (
    <React.StrictMode>
      <AppRouter />
    </React.StrictMode>
  );
}

export default App;
