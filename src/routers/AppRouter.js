import { BrowserRouter, Route, Routes } from "react-router-dom";
import { FavoritosScreen } from "../components/favoritos/FavoritosScreen";
import { HomeScreen } from "../components/home/HomeScreen";
import { PersonajeScreen } from "../components/personaje/PersonajeScreen";
import { Navbar } from "../components/ui/Navbar";

export const AppRouter = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <div className="container">
        <Routes>
          <Route path="/" element={<HomeScreen />} />
          <Route path="/home" element={<HomeScreen />} />
          <Route path="/favoritos" element={<FavoritosScreen />} />
          <Route path="/personaje" element={<PersonajeScreen />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}